//!Synchronization primitives working across tasks on a single threaded executor
//!=============================================================================
//!
//!Thanks to Rust amazing safety guaranties, it is relatively easy to implement synchronisation primitives that work across tasks in a single threaded executor
//!
//!Thoses primitive do not implement `Send` and are meant to be used with tokio's [spawn_local](https://docs.rs/tokio/1.6.0/tokio/task/fn.spawn_local.html) or something similar from an other executor
//!
//!As a result, they are written in 100% safe Rust, and could be optimized to provide better performance compared to other primitives that are `Sync` (note: this hasn't been benchmarked)
//!
//!This was created as a small project to get familiar with the inner workings of async in Rust, you're likely better off with using what's available in mainstream async libraries.

mod mutex;
pub use mutex::{Mutex, MutexGuard};
