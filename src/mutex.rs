use std::cell::{Cell, RefCell, RefMut};
use std::collections::VecDeque;
use std::future::Future;
use std::ops::{Deref, DerefMut};
use std::pin::Pin;
use std::task::{
    Context,
    Poll::{self, *},
    Waker,
};

/// A Mutex to use for synchronization across asynchronous task in a single threaded executor
///
/// As such, it doesn't implement the Sync trait
/// The intended use case for this Mutex is to share it across task spawned locally
/// with tokio's [spawn_local](https://docs.rs/tokio/0.2.22/tokio/task/fn.spawn_local.html)
/// or something similar from an other executor
///
/// ```
/// # #[tokio::test]
/// # async fn basic_usage() {
/// use std::time::Duration;
/// use tokio::task;
/// use tokio::time;
///
/// let local = task::LocalSet::new();
/// let mu = Rc::new(Mutex::new(50u32));
/// let mu_cl = mu.clone();
/// let mu_cl2 = mu.clone();
/// local.spawn_local(async move {
///     let mut guard = mu_cl.lock().await;
///     assert_eq!(50, *guard);
///     *guard = 60;
///     assert_eq!(60, *guard);
///     time::timeout(Duration::from_millis(200), async {})
///         .await
///         .unwrap();
/// });
///
/// local.spawn_local(async move {
///     time::timeout(Duration::from_millis(100), async {})
///         .await
///         .unwrap();
///     let mut guard = mu_cl2.lock().await;
///     assert_eq!(60, *guard);
///     *guard = 70;
///     assert_eq!(70, *guard);
/// });
///
/// local.await;
/// assert_eq!(70, *mu.try_lock().unwrap());
/// # }
/// ```
///
/// Fairness
/// ========
///
/// Currently, fairness is not fully guaranted.
pub struct Mutex<T> {
    is_locked: Cell<bool>,
    wait_counter: Cell<u64>,
    data: RefCell<T>,
    waiting: RefCell<VecDeque<Option<Waiting>>>,
}

/// Wakers to a task that is waiting for the mutex to be free
///
/// A `VecDeque` is used to store the Waiting wakers. Therefore, they are identified by an id
struct Waiting {
    waker: Waker,
    id: u64,
}

impl<T> Mutex<T> {
    /// Creates a new Mutex
    pub fn new(data: T) -> Mutex<T> {
        Self {
            is_locked: Cell::new(false),
            wait_counter: Cell::new(0),
            data: RefCell::new(data),
            waiting: RefCell::new(VecDeque::new()),
        }
    }

    /// Try locking a Mutex, returning immediately with `Err`
    /// if the mutex is already locked
    ///
    /// Can be used outside of an async context
    pub fn try_lock(&self) -> Result<MutexGuard<'_, T>, ()> {
        if !self.is_locked.get() {
            self.is_locked.set(true);
            Ok(MutexGuard {
                mu: self,
                data: self.data.borrow_mut(),
            })
        } else {
            Err(())
        }
    }

    /// Obtain a WaitingGuard, which can then be polled to obtain a lock on the Mutex
    fn acquire(&self) -> WaitingGuard<'_, T> {
        let counter = self.wait_counter.get();
        self.wait_counter.set(counter + 1);
        WaitingGuard {
            mu: self,
            id: counter + 1,
        }
    }

    /// Obtain a lock on the Mutex
    pub async fn lock(&self) -> MutexGuard<'_, T> {
        self.acquire().await
    }

    /// Try to get a lock on the Mutex
    /// If the Mutex is already locked, returns `Pending` and registers the waker to be waked once
    /// the Mutex is free
    fn poll_lock(&self, cx: &mut Context, id: u64) -> Poll<MutexGuard<'_, T>> {
        if !self.is_locked.get() {
            self.is_locked.set(true);
            Ready(MutexGuard {
                mu: self,
                data: self.data.borrow_mut(),
            })
        } else {
            let mut all_waiting = self.waiting.borrow_mut();
            for waiting in all_waiting.iter_mut().filter_map(|m| m.as_mut()) {
                if waiting.id == id {
                    if !cx.waker().will_wake(&waiting.waker) {
                        waiting.waker = cx.waker().clone();
                    }
                    return Pending;
                }
            }
            all_waiting.push_back(Some(Waiting {
                waker: cx.waker().clone(),
                id,
            }));

            Pending
        }
    }

    /// Drop the lock
    fn drop_lock(&self) {
        let is_locked = self.is_locked.replace(false);
        if !is_locked {
            panic!("unlocking an unlocked mutex");
        }

        while let Some(waiting_opt) = self.waiting.borrow_mut().pop_front() {
            if let Some(w) = waiting_opt {
                w.waker.wake();
            }
        }
    }

    /// Drop a waiting guard
    fn drop_waiting(&self, id: u64) {
        for waiting_opt in self.waiting.borrow_mut().iter_mut() {
            if let Some(w) = waiting_opt {
                if w.id == id {
                    *waiting_opt = None;
                    return;
                }
            }
        }
    }
}

/// Future waiting that is `Ready` once the lock is acquired
struct WaitingGuard<'m, T> {
    mu: &'m Mutex<T>,
    id: u64,
}

impl<'m, T> Future for WaitingGuard<'m, T> {
    type Output = MutexGuard<'m, T>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context) -> Poll<Self::Output> {
        self.mu.poll_lock(cx, self.id)
    }
}

impl<'m, T> Drop for WaitingGuard<'m, T> {
    fn drop(&mut self) {
        self.mu.drop_waiting(self.id);
    }
}

impl<'m, T> Drop for MutexGuard<'m, T> {
    fn drop(&mut self) {
        self.mu.drop_lock();
    }
}

/// Handle to a held `Mutex`
pub struct MutexGuard<'m, T> {
    mu: &'m Mutex<T>,
    data: RefMut<'m, T>,
}

impl<'m, T> Deref for MutexGuard<'m, T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        &*self.data
    }
}

impl<'m, T> DerefMut for MutexGuard<'m, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut *self.data
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::mem::drop;
    use std::rc::Rc;

    #[tokio::test]
    async fn simple() {
        let mu = Mutex::new(50u32);
        let mut guard = mu.lock().await;
        assert_eq!(50, *guard);
        *guard = 60;
        assert_eq!(60, *guard);

        drop(guard);

        let guard_bis = mu.lock().await;
        assert_eq!(60, *guard_bis);
    }
    #[tokio::test]
    async fn multitask() {
        use std::time::Duration;
        use tokio::task;
        use tokio::time;

        let local = task::LocalSet::new();
        let mu = Rc::new(Mutex::new(50u32));
        let mu_cl = mu.clone();
        let mu_cl2 = mu.clone();
        local.spawn_local(async move {
            let mut guard = mu_cl.lock().await;
            assert_eq!(50, *guard);
            *guard = 60;
            assert_eq!(60, *guard);
            time::timeout(Duration::from_millis(200), async {})
                .await
                .unwrap();
        });

        local.spawn_local(async move {
            time::timeout(Duration::from_millis(100), async {})
                .await
                .unwrap();
            let mut guard = mu_cl2.lock().await;
            assert_eq!(60, *guard);
            *guard = 70;
            assert_eq!(70, *guard);
        });

        local.await;
        assert_eq!(70, *mu.try_lock().unwrap());
    }
}
